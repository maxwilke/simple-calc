#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2)
{
	return num1 + num2;
}

float Subtract(float num1, float num2)
{
	return num1 - num2;
}

float Multiply(float num1, float num2)
{
	return num1 * num2;
}

bool Divide(float num1, float num2, float &answer)
{

	if (num2 == 0)
	{
		return false;
	}
	else
	{
		answer = num1 / num2;
		return true;
	}

}
//
//float Pow(float num1, int num2)
//{
//
//
//	if (num2 > 0)
//	{
//		Pow(num1 *= num1, num2 - 1);
//	}
//	return num1;
//}

int main()
{
	float usernum1;
	float usernum2;
	string symbol;
	bool keep_going = true;
	float answer;

	while (keep_going)
	{
		cout << "This is a simple calculator. Please enter two positive numbers and either \"+,-,/,*,^\".\n";
		cout << "Please enter your first number.";
		cin >> usernum1;

		cout << "Please enter your second number.";
		cin >> usernum2;

		cout << "Please enter how you\'d like these numbers to be treated.";
		cin >> symbol;

		if (usernum1 >= 0 && usernum2 >= 0)
		{
			if (symbol == "+")
			{
				answer = Add(usernum1, usernum2);
				cout << "The answer is " << answer << "\n";
			}
			else if (symbol == "-")
			{
				answer = Subtract(usernum1, usernum2);
				cout << "The answer is " << answer << "\n";
			}
			else if (symbol == "*")
			{
				answer = Multiply(usernum1, usernum2);
				cout << "The answer is " << answer << "\n";
			}
			else if (symbol == "/")
			{
				if (Divide(usernum1, usernum2, answer) == false)
				{
					cout << "Can not divide by zero.\n";
				}
				else
				{
					cout << "The answer is " << answer << "\n";
				}

			}
			//else if (symbol == "^")
			//{			
			//	answer = Pow(usernum1, static_cast<int>(usernum2));
			//	cout << "The answer is " << answer << "\n";
			//}
			else
			{
				cout << "Please use only the symbols shown above.\n";
			}

		}
		else
		{
			cout << "Please use only positive numbers.";
		}
		cout << "Type 1 and enter to do another calculation. Or 0 and enter to quit.";
		cin >> keep_going;
	}


	(void)_getch();
	return 0;
}